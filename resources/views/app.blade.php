<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="theme-color" content="#f5f5f5"/>
	<title>Ajax Laravel CRUD</title>
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
	<script>
		WebFont.load({
			google: {
				families: ['Open Sans:800', 'Nunito:600']
			}
		});
	</script>
	<link rel="stylesheet" href="{{ asset('css/app2.css') }}">
	<link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/animate.css') }}">

    @include('includes.head')
</head>
<body class="fixed-left widescreen">

<div id="wrapper" >
    @include('includes.header')

    <div class="content-page">
        <!-- Start content -->
    @yield('body')

    <!-- content -->

        <footer class="footer">
            @include('includes.footer')
        </footer>

    </div>

    @include('includes.footerjs')

</body>
</html>
