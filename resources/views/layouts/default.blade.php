<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    @include('includes.head')
</head>


<body class="fixed-left widescreen">

<div id="wrapper" >
    {{--<div id="loading">--}}
        {{--<div id="loading-center">--}}
            {{--<div id="loading-center-absolute">--}}
                {{--<div id="object"></div>--}}
            {{--</div>--}}
        {{--</div>--}}

    {{--</div>--}}
    <div id="loading">
        <div id="loading-center">
            <div id="loading-center-absolute">
                <div class="object" id="object_one"></div>
                <div class="object" id="object_two"></div>
                <div class="object" id="object_three"></div>
                <div class="object" id="object_four"></div>
                <div class="object" id="object_five"></div>
                <div class="object" id="object_six"></div>
                <div class="object" id="object_seven"></div>
                <div class="object" id="object_eight"></div>
                <div class="object" id="object_big"></div>
            </div>
        </div>

    </div>
    @include('includes.header')

    <div class="content-page">
        <!-- Start content -->
    @yield('content')

    <!-- content -->

        <footer class="footer">
            @include('includes.footer')
        </footer>

    </div>

@include('includes.footerjs')


</body>
</html>
