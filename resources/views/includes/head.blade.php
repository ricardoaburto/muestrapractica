<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
<meta name="author" content="Coderthemes">

<link rel="shortcut icon" href="http://coderthemes.com/flacto_1.5/blue_1_light/assets/images/favicon.ico">

<title> Dashboard Template</title>
<!--Morris Chart CSS -->

<link rel="stylesheet" href="{{ asset('flacto/plugins/morris/morris.css') }}">
<link href="{{ asset('flacto/css/bootstrap.min.css') }}" rel="stylesheet"
      type="text/css">
<link href="{{ asset('flacto/css/style.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('flacto/css/menu.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('flacto/css/core.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('flacto/css/components.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('flacto/css/icons.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('flacto/css/pages.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('flacto/css/fixes.css') }}" rel="stylesheet" type="text/css">





<link href="{{ asset('flacto/plugins/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('flacto/plugins/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('flacto/plugins/datatables/fixedHeader.bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('flacto/plugins/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet"
      type="text/css">
<link href="{{ asset('flacto/plugins/datatables/scroller.bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('js/plugins/gritter/jquery.gritter.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('js/plugins/core/blackout.css') }}" rel="stylesheet" type="text/css">

<link href="{{ asset('flacto/plugins/chosen/chosen.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('flacto/plugins/multiselect/css/multi-select.css') }}" rel="stylesheet" type="text/css">
