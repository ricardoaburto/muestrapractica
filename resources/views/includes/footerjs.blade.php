<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->

<script src="{{ asset('flacto/js/jquery.min.js') }}"></script>
<script src="{{ asset('js/plugins/gritter/jquery.gritter.min.js') }}"></script>
<script src="{{ asset('flacto/plugins/fancytree/jquery-ui-dependencies/jquery-ui.js') }}"></script>
<script src="{{ asset('flacto/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('flacto/js/detect.js') }}"></script>
<script src="{{ asset('flacto/js/fastclick.js') }}"></script>
<script src="{{ asset('flacto/js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('flacto/js/jquery.blockUI.js') }}"></script>
<script src="{{ asset('flacto/js/waves.js') }}"></script>
<script src="{{ asset('flacto/js/wow.min.js') }}"></script>
<script src="{{ asset('flacto/js/jquery.nicescroll.js') }}"></script>
<script src="{{ asset('flacto/js/jquery.scrollTo.min.js') }}"></script>


<script src="{{ asset('flacto/plugins/checktree/jquery-checktree.js') }}"></script>
<script src="{{ asset('flacto/plugins/validate/dist/jquery.validate.js') }}"></script>
<script src="{{ asset('flacto/plugins/validate/lib/jquery.mockjax.js') }}"></script>
<script src="{{ asset('flacto/plugins/validate/lib/jquery.form.js') }}"></script>

<!-- Counter Up  -->
<script src="{{ asset('flacto/plugins/waypoints/lib/jquery.waypoints.js') }}"></script>
<script src="{{ asset('flacto/plugins/counterup/jquery.counterup.min.js') }}"></script>


<!-- App js -->


<script src="{{ asset('flacto/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('flacto/plugins/datatables/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('flacto/plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('flacto/plugins/datatables/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('flacto/plugins/datatables/jszip.min.js') }}"></script>
<script src="{{ asset('flacto/plugins/datatables/pdfmake.min.js') }}"></script>
<script src="{{ asset('flacto/plugins/datatables/vfs_fonts.js') }}"></script>
<script src="{{ asset('flacto/plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ asset('flacto/plugins/datatables/buttons.print.min.js') }}"></script>
<script src="{{ asset('flacto/plugins/datatables/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{ asset('flacto/plugins/datatables/dataTables.keyTable.min.js') }}"></script>
<script src="{{ asset('flacto/plugins/datatables/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('flacto/plugins/datatables/responsive.bootstrap.min.js') }}"></script>
<script src="{{ asset('flacto/plugins/datatables/dataTables.scroller.min.js') }}"></script>


<script src="{{ asset('flacto/pages/jquery.datatables.init.js') }}"></script>


<script src="{{ asset('flacto/plugins/fancytree/jquery.fancytree-all.min.js') }}"></script>
<script src="{{ asset('flacto/plugins/Collapsible-Tree-View-Checkboxes-jQuery-hummingbird/hummingbird-treeview.js') }}"></script>


<script src="{{ asset('flacto/plugins/validador_rut/jquery-validate-rut.js') }}"></script>
<script src="{{ asset('js/plugins/rut/jquery.Rut.min.js') }}"></script>

<script src="{{ asset('js/plugins/core/blackout.js') }}"></script>
<script src="{{ asset('js/plugins/core/xmodal.js') }}"></script>


<script src="{{ asset('flacto/js/jquery.core.js') }}"></script>


<script src="{{ asset('flacto/plugins/multiselect/bootstrap-multiselect.js') }}"></script>
<script src="{{ asset('flacto/plugins/multiselect/js/jquery.multi-select.js') }}"></script>
<script src="{{ asset('flacto/plugins/chosen/chosen.jquery.js') }}"></script>


<script src="{{ asset('flacto/js/jquery.app.js') }}"></script>
<script src="{{ asset('js/init.js') }}"></script>

@php
    $URL = explode('/',Request::path());
@endphp

@if(Request::path() == "home")

@else
    <script type="text/javascript">
        App.BASE_URL = "{{Request::root()}}";

    </script>
    <script src="{{ asset('js/'.Request::path().'/'.$URL[1].'.js') }}"></script>
@endif

{{--<script src="{{ asset('js/empresa/empresa.js') }}"></script>--}}
{{--<script src="{{ asset('js/usuario/usuario.js') }}"></script>--}}
<script type="text/javascript">
    $(window).load(function () {
        $("#loading-center").click(function () {
            $("#loading").fadeOut(500);
        })

    });
    $(window).load(function () {
        $("#loading").fadeOut(500);
    })

</script>
<script type="text/javascript">


</script>

