@extends('app')

@section('body')
    @inject('venta','App\Http\Controllers\Venta')

    <!-- Start content -->
    <div class="content">
        <div class="container">
            <section id="content" class="animated fadeIn">
                <div class="row full-row" data-maincontainerform style="display: none;">
                    <div class="col-lg-12">
                        <div class="card-box" id="p0" data-panel-title="false" data-panel-fullscreen="false"
                             data-panel-remove="false" data-panel-collapse="false">
                            <form class="form-horizontal" action="#" id="primal-form">
                                <div class="m-b-30">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="panel panel-default bx-shadow-none panel-min-height">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title" style="color:#797979;">Venta
                                                    </h3>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="form-group">
                                                        <div class="col-sm-3">
                                                            <label class="col-sm-4 control-label">Cliente</label>
                                                            <div class="col-sm-8">
                                                                <div class="bs-component">
                                                                    <select class="form-control m-b chosen-select"
                                                                            name="idcliente"
                                                                            data-placeholder="Seleccione un Cliente"
                                                                            required="required">
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">

                                                        <div class="col-sm-3">
                                                            <label class="col-sm-4 control-label">Producto</label>
                                                            <div class="col-sm-8">
                                                                <div class="bs-component">
                                                                    <select class="form-control m-b chosen-select" name="idproducto"
                                                                            data-placeholder="Seleccione Producto">
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-3">
                                                            <label class="col-sm-4 control-label">precio</label>
                                                            <div class="col-sm-8">
                                                                <div class="bs-component">
                                                                    <input type="text" class="form-control text-right" data-numbercharacters
                                                                           name="precio" value="0">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <label class="col-sm-4 control-label">Cantidad</label>
                                                            <div class="col-sm-8">
                                                                <div class="bs-component">
                                                                    <input type="number" class="form-control text-right"
                                                                           name="cantidad" min="0"
                                                                           value="0" >
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                                <button style="margin-top: 3px;"
                                                                        class="btn btn-primary waves-effect waves-light"
                                                                        onclick="App.addProducto();return false;">
                                                                    Agregar
                                                                </button>
                                                        </div>

                                                    </div>
                                                    <div class="form-group">
                                                        <table class="table table-striped m-0" id="tabla_venta"
                                                               style="border: 1px solid #ddd;"
                                                        >
                                                            <thead>
                                                            <tr>
                                                                <th style="width: 5%">N°</th>
                                                                <th style="width: 40%">Producto</th>
                                                                <th style="width: 40%">Cantidad</th>
                                                                <th style="width: 40%">Precio</th>
                                                                <th style="width: 15%">#</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            </tbody>
                                                        </table>

                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-4">
                                                        </div>
                                                        <div class="col-sm-4">
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label class="col-sm-4 control-label">Subtotal:</label>
                                                            <div class="col-sm-8">
                                                                <div class="bs-component">
                                                                    <p class="col-sm-12 control-label"
                                                                       style="padding-right: 25px;" name="subtotal">$
                                                                        0</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-4">
                                                        </div>
                                                        <div class="col-sm-4">
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label class="col-sm-4 control-label">Descuento</label>
                                                            <div class="col-sm-8">
                                                                <div class="bs-component">
                                                                    <input type="number" value="0"
                                                                           class="form-control text-right"
                                                                           name="descuento"
                                                                           placeholder="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-4">
                                                        </div>
                                                        <div class="col-sm-4">
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label class="col-sm-4 control-label">IVA:</label>
                                                            <div class="col-sm-8">
                                                                <div class="bs-component">
                                                                    <p class="col-sm-12 control-label"
                                                                       style="padding-right: 25px;" name="iva">$ 0</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-4">
                                                        </div>
                                                        <div class="col-sm-4">
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label class="col-sm-4 control-label">Total:</label>
                                                            <div class="col-sm-8">
                                                                <div class="bs-component">
                                                                    <p class="col-sm-12 control-label"
                                                                       style="padding-right: 25px;" name="total">$ 0</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <input type="hidden" name="idventa" id="idventa" value="0" class="pk_form"/>
                                <div class="text-right">
                                    <a class="btn btn-inverse waves-effect waves-light" href="#" data-cancel_form>Cancelar</a>
                                    <button class="btn btn-primary waves-effect waves-light"  type="submit">
                                        Guardar
                                    </button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>

                <div class="row full-row">
                    <div class="col-md-12">
                        <div class="card-box table-responsive" id="spy2">
                            <div class="pull-right">
                                <button type="button" class="btn btn-primary waves-effect waves-light"
                                        onclick="" data-addnew>Procesar venta
                                </button>
                            </div>
                            <h4 class="header-title m-b-30">Listado de Ventas</h4>
                            @include('admin/venta.tableventa')

                        </div>
                    </div>
                </div>
            </section>
        </div>
        <br>
    </div>

@endsection
