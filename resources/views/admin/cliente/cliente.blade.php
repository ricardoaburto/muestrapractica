@extends('app')

@section('body')
sdfd
    @inject('cliente','App\Http\Controllers\Cliente')

    <!-- Start content -->
    <div class="content">
        <div class="container">
        </div>
        <br>

        <div id="table-loader" style="display: none">
            <div class="col-md-12">
                <div class="no-items loader-height">
                    <div class="no-items-wrapper">
                        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    <div class=" pull-right">
                        <button type="button" class="btn btn-custom waves-effect waves-light" data-toggle="modal"
                                data-target="#create-item">
                            Agregar
                        </button>
                    </div>
                    <h4 class="header-title m-t-0 m-b-30">Listado de Cliente</h4>
                    <div class="table-container">
                        @include('admin/cliente.tablecliente')
                    </div>
                </div>
            </div>
        </div>


        <!-- Create item modal -->
        <div class="modal fade" id="create-item" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Crear Cliente</h4>
                    </div>
                    <form action="cliente" method="post" id="store-form">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12" style="margin-bottom: 30px;">

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <label class="control-label" for="nombre">Nombre</label>
                                                <div class="mi-componente">
                                                    <input type="text" name="nombre" class="form-control"
                                                           placeholder="Nombre" id="store-nombre">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="control-label">Apellido</label>
                                                <div class="mi-componente">
                                                    <input type="text" name="apellido" class="form-control"
                                                           placeholder="Apellido" id="store-apellido">
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <label class="control-label">Rut</label>
                                                <div class="mi-componente">
                                                    <input type="text" class="form-control" name="rut"
                                                           oninput="checkRut(this)"
                                                           placeholder="RUT"
                                                           maxlength="12" id="store-rut">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="control-label" for="example-email">Email</label>
                                                <div class="mi-componente">
                                                    <input type="email" id="store-email" name="email"
                                                           class="form-control"
                                                           placeholder="Email">
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <label class="control-label">Perfil</label>
                                                <div class="mi-componente">
                                                    <select class="form-control" name="idperfil"
                                                            id="store-idperfil">

                                                    </select>
                                                    {{--<select>--}}
                                                    {{--@foreach($cliente->links() as $link)--}}
                                                    {{--<option value="{{ $link['idperfil'] }}">{{ $link['name'] }}</option>--}}
                                                    {{--@endforeach--}}
                                                    {{--</select>--}}
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="control-label">Repetir Contraseña</label>
                                                <div class="mi-componente">
                                                    <input type="password" class="form-control" name="password"
                                                           placeholder="Repetir Contraseña" id="store-pass">
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <label class="control-label">Repetir Contraseña</label>
                                                <div class="mi-componente">
                                                    <input type="password" class="form-control" name="rpass"
                                                           placeholder="Repetir Contraseña" id="store-rpass">
                                                </div>

                                            </div>
                                            <div class="col-sm-3">
                                                <div class="checkbox checkbox-primary" style="margin-top: 30px;">
                                                    <input id="store-correspondencia" type="checkbox"
                                                           name="correspondencia">
                                                    <label for="store-correspondencia">
                                                        Correspondencia
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-3 show_correspondencia" style="display: none">
                                                <div class="checkbox checkbox-primary" style="margin-top: 30px;">
                                                    <input id="store-acopio" type="checkbox" name="acopio">
                                                    <label for="store-acopio">
                                                        Acopio
                                                    </label>
                                                </div>
                                            </div>


                                        </div>


                                    </div>
                                    <div class="col-sm-12 show_correspondencia">
                                        <div class="col-sm-6">
                                            <div class="card-box" style="margin-top: 30px;">

                                                <div id="carga_arbol" style="overflow: auto;max-height: 300px;min-height: 200px"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="card-box" style="margin-top: 30px;">

                                                <div id="carga_arbol2" style="overflow: auto;max-height: 300px;min-height: 200px"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success" id="store-submit">Crear
                                </button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                    Cerrar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Update item modal -->
        <div class="modal fade" id="edit-item" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Actualizar Cliente</h4>
                    </div>
                    <div class="modal-body" id="edit-loading-bar">
                        <div class="progress">
                            <div class="progress-bar progress-bar-success progress-bar-striped active loading-bar"
                                 role="progressbar" aria-valuenow="100">
                            </div>
                        </div>
                    </div>
                    <form method="post" id="update-form">
                        {{ method_field('PATCH') }}
                        <input type="hidden" name="id" id="update-id">
                        <input type="hidden" name="remember_token" id="update-remember_token">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-sm-12" style="margin-bottom: 30px;">

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <label class="control-label" for="nombre">Nombre</label>
                                                <div class="mi-componente">
                                                    <input type="text" name="nombre" class="form-control"
                                                           placeholder="Nombre" id="update-nombre">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="control-label">Apellido</label>
                                                <div class="mi-componente">
                                                    <input type="text" name="apellido" class="form-control"
                                                           placeholder="Apellido" id="update-apellido">
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <label class="control-label">Rut</label>
                                                <div class="mi-componente">
                                                    <input type="text" class="form-control" name="rut"
                                                           oninput="checkRut(this)"
                                                           placeholder="RUT"
                                                           maxlength="12" id="update-rut">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="control-label" for="example-email">Email</label>
                                                <div class="mi-componente">
                                                    <input type="email" id="update-email" name="email"
                                                           class="form-control"
                                                           placeholder="Email">
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <label class="control-label">Perfil</label>
                                                <div class="mi-componente">
                                                    <select class="form-control" name="idperfil"
                                                            id="update-idperfil">

                                                    </select>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                    <dic class="col-sm-12" style="margin-top: 15px;">
                                        <div class="alert alert-info">
                                            Si no Ingresa una contraseña se mantendra la anterior.
                                        </div>
                                    </dic>
                                    <div class="col-sm-12">

                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <label class="control-label">Repetir Contraseña</label>
                                                <div class="mi-componente">
                                                    <input type="password" class="form-control" name="pass"
                                                           placeholder="Repetir Contraseña" id="update-pass">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">

                                                <label class="control-label">Repetir Contraseña</label>
                                                <div class="mi-componente">
                                                    <input type="password" class="form-control" name="rpass"
                                                           placeholder="Repetir Contraseña" id="update-rpass">
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <div class="checkbox checkbox-primary" style="margin-top: 30px;">
                                                    <input id="update-correspondencia" type="checkbox"
                                                           name="correspondencia">
                                                    <label for="update-correspondencia">
                                                        Correspondencia
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 show_correspondencia_edit" style="display: none">
                                                <div class="checkbox checkbox-primary" style="margin-top: 30px;">
                                                    <input id="update-acopio" type="checkbox" name="acopio">
                                                    <label for="update-acopio">
                                                        Acopio
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-sm-12 show_correspondencia_edit">
                                        <div class="col-sm-6">
                                            <div class="card-box" style="margin-top: 30px;">

                                                <div id="carga_arbol3" style="overflow: auto;max-height: 300px;min-height: 200px"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="card-box" style="margin-top: 30px;">

                                                <div id="carga_arbol4" style="overflow: auto;max-height: 300px;min-height: 200px"></div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success" id="update-submit">Actualizar
                                </button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                    Cerrar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Destroy item modal -->
        <div class="modal fade" id="destroy-item" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Eliminar Cliente</h4>
                    </div>
                    <div class="modal-body" id="destroy-loading-bar">
                        <div class="progress">
                            <div class="progress-bar progress-bar-success progress-bar-striped active loading-bar"
                                 role="progressbar" aria-valuenow="100">
                            </div>
                        </div>
                    </div>
                    <div class="modal-body" id="remove-data">
                        <h3 class="text-center">Esta seguro que desea eliminar este Cliente</h3>
                    </div>
                    <div class="modal-footer">
                        <form method="post" id="remove-form">
                            {{ method_field('DELETE') }}
                            <input type="hidden" name="id" id="remove-id">
                            <button type="submit" class="btn btn-danger">Eliminar</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                Cerrar
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
    </div>
    </div>
    </div> <!-- container -->

    </div> <!-- content -->
    </div> <!-- content -->
@endsection
