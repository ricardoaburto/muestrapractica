jQuery(document).ready(function ($) {
    $("#store-form").validate({
        rules: {
            nombre: {
                required: true
            }
        },
        messages: {
            nombre: {
                required: "Debe ingresar un(a) Nombre"
            }

        },
        highlight: function (element) {

            $(element).closest('.mi-componente').removeClass('has-success').addClass('has-error');

        },
        success: function (element) {

            $(element).closest('.mi-componente').removeClass('has-error').find('label.error').remove();
        },
        errorPlacement: function (error, element) {
            if ($(element).is('select.chosen-select')) {
                $(element).closest('.mi-componente').append(error);
            } else if ($(element).is('select[multiple]')) {
                $(element).closest('div').parent().closest('div').append(error);
            } else {
                element.after(error);
            }
        },
        submitHandler: function (form) {
            // e.preventDefault();

            $.ajax({
                type: "POST",
                url: "cliente",
                dataType: 'json',
                data: $(form).serialize(),
                beforeSend: function () {
                    $('.error-list').remove();
                    $("#store-submit").prop("disabled", true);
                    $("#store-submit").html('<i class="loading fa fa-refresh fa-spin fa-3x fa-fw"></i><span class="sr-only loading-fallback">Loading...</span>');
                }, statusCode: {
                    500: function () {
                        $.notify({
                            // options
                            icon: 'fa fa-exclamation-triangle',
                            title: '<strong>Error 500</strong>: <br>',
                            message: 'An error occurred while sending data.'
                        }, {
                            // settings
                            type: "danger",
                            allow_dismiss: true,
                            newest_on_top: true,
                            showProgressbar: false,
                            placement: {
                                from: "top",
                                align: "right"
                            },
                            offset: 20,
                            spacing: 10,
                            z_index: 9999,
                            delay: 5000,
                            timer: 1000,
                            mouse_over: "pause",
                            animate: {
                                enter: 'animated fadeInDown',
                                exit: 'animated fadeOutUp'
                            }
                        });
                    }
                }, success: function (data) {
                    $.notify({
                        // options
                        icon: 'fa fa-check',
                        title: '<strong>Success</strong>: <br>',
                        message: data['msg']
                    }, {
                        // settings
                        type: "success",
                        allow_dismiss: true,
                        newest_on_top: true,
                        showProgressbar: false,
                        placement: {
                            from: "top",
                            align: "right"
                        },
                        offset: 20,
                        spacing: 10,
                        z_index: 9999,
                        delay: 2000,
                        timer: 1000,
                        mouse_over: "pause",
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        }
                    });

                    $("#store-nombre").val('');
                    $("#store-apellido").val('');
                    $("#store-rut").val('');
                    $("#store-email").val('');
                    $("#store-pass").val('');

                    $('#create-item').modal('toggle').click();

                    // refresh data
                    refreshTable();
                }, error: function (data) {
                    $errors = data.responseJSON.errors;
                    console.log($errors);
                    var id = '';
                    for (var i in $errors) {
                        id += "store-" + i;
                        (function () {
                            var error = $("<label for='" + id + "' class='error-list'>" + $errors[i] + "</label>");
                            error.hide().fadeIn("slow");
                            error.insertAfter('#' + id);
                        })();
                        id = '';
                    }
                }, complete() {
                    $(".loading").remove();
                    $(".loading-fallback").remove();
                    $("#store-submit").text('Create Item');
                    $("#store-submit").prop("disabled", false);
                }
            });
        }
    });
});
