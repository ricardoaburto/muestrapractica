jQuery(document).ready(function ($) {
    $('#edit-item').on('hidden.bs.modal', function () {
        $("#update-id").val('');
        $("#update-title").val('');
        $("#update-item").val('');
        $('.error-list').remove();
        $('#item-not-found').remove();
        $('#update-form').hide();
        $('#edit-loading-bar').show();

    });
    //Ajax update

    $("#update-form").validate({
        rules: {
            nombre: {
                required: true
            }
        },
        messages: {
            nombre: {
                required: "Debe ingresar un(a) Nombre"
            }
        },
        highlight: function (element) {
            $(element).closest('.mi-componente').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.mi-componente').removeClass('has-error').find('label.error').remove();
        },
        errorPlacement: function (error, element) {
            if ($(element).is('select.chosen-select')) {
                $(element).closest('.mi-componente').append(error);
            } else if ($(element).is('select[multiple]')) {
                $(element).closest('div').parent().closest('div').append(error);
            } else {
                element.after(error);
            }
        },
        submitHandler: function (form) {
            // e.preventDefault();

            $.ajax({
                type: "POST",
                url: "usuario/" + $('#update-id').attr("value"),
                dataType: 'json',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: $(form).serialize(),
                beforeSend: function () {
                    $('.error-list').remove();
                    $("#store-submit").prop("disabled", true);
                    $("#store-submit").html('<i class="loading fa fa-refresh fa-spin fa-3x fa-fw"></i><span class="sr-only loading-fallback">Loading...</span>');
                }, statusCode: {
                    500: function () {
                        $.notify({
                            // options
                            icon: 'fa fa-exclamation-triangle',
                            title: '<strong>Error 500</strong>: <br>',
                            message: 'An error occurred while sending data.'
                        }, {
                            // settings
                            type: "danger",
                            allow_dismiss: true,
                            newest_on_top: true,
                            showProgressbar: false,
                            placement: {
                                from: "top",
                                align: "right"
                            },
                            offset: 20,
                            spacing: 10,
                            z_index: 9999,
                            delay: 5000,
                            timer: 1000,
                            mouse_over: "pause",
                            animate: {
                                enter: 'animated fadeInDown',
                                exit: 'animated fadeOutUp'
                            }
                        });
                    }
                }, success: function (data) {


                    $.notify({
                        // options
                        icon: 'fa fa-check',
                        title: '<strong>Success</strong>: <br>',
                        message: data['msg']
                    }, {
                        // settings
                        type: "success",
                        allow_dismiss: true,
                        newest_on_top: true,
                        showProgressbar: false,
                        placement: {
                            from: "top",
                            align: "right"
                        },
                        offset: 20,
                        spacing: 10,
                        z_index: 9999,
                        delay: 2000,
                        timer: 1000,
                        mouse_over: "pause",
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        }
                    });

                    $("#store-nombre").val('');
                    $("#store-apellido").val('');
                    $("#store-rut").val('');
                    $("#store-email").val('');
                    $("#store-pass").val('');

                    $('#edit-item').modal('toggle').click();


                    $('#tabla_cliente').dataTable().fnDestroy();
                    $('#tabla_cliente').dataTable({
                        paging: false,
                        bDestroy: true,
                        searching: false,
                        bPaginate: false,
                        bLengthChange: true,
                        bFilter: false,
                        bSort: false,
                        sServerMethod: "GET",
                        ajax: {
                            type: "GET",
                            url: 'table'
                        },
                        bInfo: false,
                        bAutoWidth: false,
                        iDisplayLength: 5,
                        // "aaData":data,
                        lengthMenu: [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "All"]],

                        datatype: "json",
                        columns: [
                            {data: "idcliente"},
                            {data: "nombre"},
                            {data: "apellido"},
                            {data: "rut"},
                            {data: "fecha_creacion"},
                            {data: "editar"},
                            {data: "eliminar"}
                        ]

                    });
                }, error: function (data) {
                    $errors = data.responseJSON.errors;
                    console.log($errors);
                    var id = '';
                    for (var i in $errors) {
                        id += "store-" + i;
                        (function () {
                            var error = $("<label for='" + id + "' class='error-list'>" + $errors[i] + "</label>");
                            error.hide().fadeIn("slow");
                            error.insertAfter('#' + id);
                        })();
                        id = '';
                    }
                }, complete() {
                    $(".loading").remove();
                    $(".loading-fallback").remove();
                    $("#update-submit").text('Update Item');
                    $("#update-submit").prop("disabled", false)
                }
            });
        }
    });
});
