jQuery(document).ready(function ($) {
    var init_form = function () {
        $.ajax({
            type: "GET",
            url: "init_form",
            dataType: 'json',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function (data) {
                //$('#store-idperfil').html(data.select).trigger('chosen:updated');
            }
        })
    }

    /*carga de los select*/
    $('#tabla_cliente').dataTable({
        paging: true,
        bDestroy: true,
        searching: true,
        bPaginate: false,
        bLengthChange: true,
        bFilter: false,
        bSort: false,
        sServerMethod: "GET",
        ajax: {
            type: "GET",
            url: 'table'
        },
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        bInfo: false,
        bAutoWidth: false,
        iDisplayLength: 5,
        lengthMenu: [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "All"]],
        datatype: "json",
        columns: [
            {data: "nombre"},
            {data: "editar"},
            {data: "eliminar"}
        ],
        "columnDefs": [
            {"width": "50px", "targets": [4, 5]},
            {"sClass": 'text-center', 'aTargets': [0, 1, 2, 3]}
        ]
    });
    /*Modal*/
    $('#edit-item').on('hidden.bs.modal', function () {
        $("#update-correspondencia").prop('checked', false);
        $('.show_correspondencia_edit').hide('show');
        $("#update-id").val('');
        $("#update-title").val('');
        $("#update-item").val('');
        $('.error-list').remove();
        $('#item-not-found').remove();
        $('#update-form').hide();
        $('#edit-loading-bar').show();
    });
    $('#create-item').on('show.bs.modal', function () {
        //limpiar creados
        init_form();
    });


    $('#destroy-item').on('hidden.bs.modal', function () {
        $('#item-not-found').remove();
        $('#remove-data').show();
    });

    $(".table-container").on("click touchstart", ".remove-btn", function () {
        $("#remove-id").val($(this).attr("value"));
    });

    recargar = function () {
        $('#tabla_cliente').dataTable().fnDestroy();
        $('#tabla_cliente').dataTable({
            paging: true,
            bDestroy: true,
            searching: true,
            bPaginate: false,
            bLengthChange: true,
            bFilter: false,
            bSort: false,
            sServerMethod: "GET",
            ajax: {
                type: "GET",
                url: 'table'
            },
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
            },
            bInfo: false,
            bAutoWidth: false,
            iDisplayLength: 5,
            lengthMenu: [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "All"]],
            datatype: "json",
            columns: [
                {data: "nombre"},
                {data: "editar"},
                {data: "eliminar"}
            ],
            "columnDefs": [
                {"width": "50px", "targets": [4, 5]},
                {"sClass": 'text-center', 'aTargets': [0, 1, 2, 3]}
            ]
        });
    }
    /*Agregar Formulario*/
    $("#store-form").validate({
        rules: {
            nombre: {
                required: true
            }

        },
        messages: {
            nombre: {
                required: "Debe ingresar un(a) Nombre"
            },
        },
        highlight: function (element) {
            $(element).closest('.mi-componente').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.mi-componente').removeClass('has-error').find('label.error').remove();
        },
        errorPlacement: function (error, element) {
            if ($(element).is('select.chosen-select')) {
                $(element).closest('.mi-componente').append(error);
            } else if ($(element).is('select[multiple]')) {
                $(element).closest('div').parent().closest('div').append(error);
            } else {
                element.after(error);
            }
        },
        submitHandler: function (form) {

            if ($('[name=correspondencia]').is(':checked') === true) {
                if (a.length > 0 && e.length > 0) {

                    $.ajax({
                        type: "POST",
                        url: "cliente",
                        dataType: 'json',
                        data: $(form).serialize(),
                        beforeSend: function () {
                            $('.error-list').remove();
                            $("#store-submit").prop("disabled", true);
                            $("#store-submit").html('<i class="loading fa fa-refresh fa-spin fa-3x fa-fw"></i><span class="sr-only loading-fallback">Loading...</span>');
                        }, statusCode: {
                            500: function () {
                                $.notify({
                                    // options
                                    icon: 'fa fa-exclamation-triangle',
                                    title: '<strong>Error 500</strong>: <br>',
                                    message: 'An error occurred while sending data.'
                                }, {
                                    // settings
                                    type: "danger",
                                    allow_dismiss: true,
                                    newest_on_top: true,
                                    showProgressbar: false,
                                    placement: {
                                        from: "top",
                                        align: "right"
                                    },
                                    offset: 20,
                                    spacing: 10,
                                    z_index: 9999,
                                    delay: 5000,
                                    timer: 1000,
                                    mouse_over: "pause",
                                    animate: {
                                        enter: 'animated fadeInDown',
                                        exit: 'animated fadeOutUp'
                                    }
                                });
                            }
                        }, success: function (data) {
                            $.notify({
                                // options
                                icon: 'fa fa-check',
                                title: '<strong>Success</strong>: <br>',
                                message: data['msg']
                            }, {
                                // settings
                                type: "success",
                                allow_dismiss: true,
                                newest_on_top: true,
                                showProgressbar: false,
                                placement: {
                                    from: "top",
                                    align: "right"
                                },
                                offset: 20,
                                spacing: 10,
                                z_index: 9999,
                                delay: 2000,
                                timer: 1000,
                                mouse_over: "pause",
                                animate: {
                                    enter: 'animated fadeInDown',
                                    exit: 'animated fadeOutUp'
                                }
                            });

                            $("#store-nombre").val('');
                            $('#create-item').modal('toggle').click();
                            recargar();
                        }, error: function (data) {
                            $errors = data.responseJSON.errors;
                            console.log($errors);
                            var id = '';
                            for (var i in $errors) {
                                id += "store-" + i;
                                (function () {
                                    var error = $("<label for='" + id + "' class='error-list'>" + $errors[i] + "</label>");
                                    error.hide().fadeIn("slow");
                                    error.insertAfter('#' + id);
                                })();
                                id = '';
                            }
                        }, complete() {
                            $(".loading").remove();
                            $(".loading-fallback").remove();
                            $("#store-submit").text('Create Item');
                            $("#store-submit").prop("disabled", false);
                        }
                    });

                } else {
                    $.notify({
                        // options
                        icon: 'fa fa-info-circle',
                        title: '<strong>Debe Seleccionar </strong>: <br>',
                        message: 'Piso y Movimiento para continuar.'
                    }, {
                        // settings
                        type: "info",
                        allow_dismiss: true,
                        newest_on_top: true,
                        showProgressbar: false,
                        placement: {
                            from: "top",
                            align: "right"
                        },
                        offset: 20,
                        spacing: 10,
                        z_index: 9999,
                        delay: 5000,
                        timer: 1000,
                        mouse_over: "pause",
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        }
                    });

                }
            } else {

                $.ajax({
                    type: "POST",
                    url: "cliente",
                    dataType: 'json',
                    data: $(form).serialize(),
                    beforeSend: function () {
                        $('.error-list').remove();
                        $("#store-submit").prop("disabled", true);
                        $("#store-submit").html('<i class="loading fa fa-refresh fa-spin fa-3x fa-fw"></i><span class="sr-only loading-fallback">Loading...</span>');
                    }, statusCode: {
                        500: function () {
                            $.notify({
                                // options
                                icon: 'fa fa-exclamation-triangle',
                                title: '<strong>Error 500</strong>: <br>',
                                message: 'An error occurred while sending data.'
                            }, {
                                // settings
                                type: "danger",
                                allow_dismiss: true,
                                newest_on_top: true,
                                showProgressbar: false,
                                placement: {
                                    from: "top",
                                    align: "right"
                                },
                                offset: 20,
                                spacing: 10,
                                z_index: 9999,
                                delay: 5000,
                                timer: 1000,
                                mouse_over: "pause",
                                animate: {
                                    enter: 'animated fadeInDown',
                                    exit: 'animated fadeOutUp'
                                }
                            });
                        }
                    }, success: function (data) {
                        $.notify({
                            // options
                            icon: 'fa fa-check',
                            title: '<strong>Success</strong>: <br>',
                            message: data['msg']
                        }, {
                            // settings
                            type: "success",
                            allow_dismiss: true,
                            newest_on_top: true,
                            showProgressbar: false,
                            placement: {
                                from: "top",
                                align: "right"
                            },
                            offset: 20,
                            spacing: 10,
                            z_index: 9999,
                            delay: 2000,
                            timer: 1000,
                            mouse_over: "pause",
                            animate: {
                                enter: 'animated fadeInDown',
                                exit: 'animated fadeOutUp'
                            }
                        });

                        $("#store-nombre").val('');
                        $("#store-apellido").val('');
                        $("#store-rut").val('');
                        $("#store-email").val('');
                        $("#store-pass").val('');
                        $('#create-item').modal('toggle').click();
                        recargar();
                    }, error: function (data) {
                        $errors = data.responseJSON.errors;
                        console.log($errors);
                        var id = '';
                        for (var i in $errors) {
                            id += "store-" + i;
                            (function () {
                                var error = $("<label for='" + id + "' class='error-list'>" + $errors[i] + "</label>");
                                error.hide().fadeIn("slow");
                                error.insertAfter('#' + id);
                            })();
                            id = '';
                        }
                    }, complete() {
                        $(".loading").remove();
                        $(".loading-fallback").remove();
                        $("#store-submit").text('Create Item');
                        $("#store-submit").prop("disabled", false);
                    }
                });


            }

        }
    });

    /*Editar formulario*/
    $("#update-form").validate({
        rules: {
            nombre: {
                required: true
            }
        },
        messages: {
            nombre: {
                required: "Debe ingresar un(a) Nombre"
            }

        },
        highlight: function (element) {
            $(element).closest('.mi-componente').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            $(element).closest('.mi-componente').removeClass('has-error').find('label.error').remove();
        },
        errorPlacement: function (error, element) {
            if ($(element).is('select.chosen-select')) {
                $(element).closest('.mi-componente').append(error);
            } else if ($(element).is('select[multiple]')) {
                $(element).closest('div').parent().closest('div').append(error);
            } else {
                element.after(error);
            }
        },
        submitHandler: function (form) {

            $.ajax({
                type: "POST",
                url: "cliente/" + $('#update-id').attr("value"),
                dataType: 'json',
                data: $(form).serialize() + '&piso=' + JSON.stringify(a) + '&movimiento=' + JSON.stringify(e),
                beforeSend: function () {
                    $('.error-list').remove();
                    $("#update-submit").prop("disabled", true);
                    $("#update-submit").html('<i class="loading fa fa-refresh fa-spin fa-3x fa-fw"></i><span class="sr-only loading-fallback">Loading...</span>');
                }, statusCode: {
                    500: function () {
                        $.notify({
                            // options
                            icon: 'fa fa-exclamation-triangle',
                            title: '<strong>Error 500</strong>: <br>',
                            message: 'An error occurred while sending data.'
                        }, {
                            // settings
                            type: "danger",
                            allow_dismiss: true,
                            newest_on_top: true,
                            showProgressbar: false,
                            placement: {
                                from: "top",
                                align: "right"
                            },
                            offset: 20,
                            spacing: 10,
                            z_index: 9999,
                            delay: 5000,
                            timer: 1000,
                            mouse_over: "pause",
                            animate: {
                                enter: 'animated fadeInDown',
                                exit: 'animated fadeOutUp'
                            }
                        });
                    }
                }, success: function (data) {
                    $.notify({
                        // options
                        icon: 'fa fa-check',
                        title: '<strong>Success</strong>: <br>',
                        message: data['msg']
                    }, {
                        // settings
                        type: "success",
                        allow_dismiss: true,
                        newest_on_top: true,
                        showProgressbar: false,
                        placement: {
                            from: "top",
                            align: "right"
                        },
                        offset: 20,
                        spacing: 10,
                        z_index: 9999,
                        delay: 2000,
                        timer: 1000,
                        mouse_over: "pause",
                        animate: {
                            enter: 'animated fadeInDown',
                            exit: 'animated fadeOutUp'
                        }
                    });
                    $("#update-nombre").val('');
                    $("#update-apellido").val('');
                    $("#update-rut").val('');
                    $("#update-email").val('');
                    $("#update-pass").val('');
                    $("#update-rpass").val('');
                    // $("#update-idperfil").val(null).trigger('chosen:updeted');
                    $('#edit-item').modal('toggle').click();
                    recargar();
                }, error: function (data) {
                    $errors = data.responseJSON.errors;
                    console.log($errors);
                    var id = '';
                    for (var i in $errors) {
                        id += "update-" + i;
                        (function () {
                            var error = $("<label for='" + id + "' class='error-list'>" + $errors[i] + "</label>");
                            error.hide().fadeIn("slow");
                            error.insertAfter('#' + id);
                        })();
                        id = '';
                    }
                }, complete() {
                    $(".loading").remove();
                    $(".loading-fallback").remove();
                    $("#update-submit").text('Actualizar');
                    $("#update-submit").prop("disabled", false)
                }
            });
        }

    });

    /*Get de los datos*/
    $(".table-container").on("click touchstart", ".edit-btn", function () {

        $.ajax({
            type: "GET",
            url: "cliente/" + $(this).attr("value") + "/edit",
            dataType: 'json',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            beforeSend: function () {
                $('#item-not-found').remove();
            },
            success: function (data) {

                $("#update-id").val(data['idcliente']);
                $('#update-form').show();
            },
            error: function (data) {
                $.notify({
                    // options
                    icon: 'fa fa-exclamation-triangle',
                    title: '<strong>Error</strong>: <br>',
                    message: 'An error occurred while getting data.'
                }, {
                    // settings
                    type: "danger",
                    allow_dismiss: true,
                    newest_on_top: true,
                    showProgressbar: false,
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 9999,
                    delay: 5000,
                    timer: 1000,
                    mouse_over: "pause",
                    animate: {
                        enter: 'animated fadeInDown',
                        exit: 'animated fadeOutUp'
                    }
                });
                $('#update-form').hide();
                (function () {
                    var notFound = $('<div class="modal-body fade-text" id="item-not-found"><h1 class="text-center danger">☠</h1><h2 class="text-center">Item not found</h2></div>');
                    notFound.insertAfter('#update-form');
                })();
            }, complete() {
                $('#edit-loading-bar').hide();
            }
        });
    });

    function refreshTable() {
        $('div.table-container').hide();
        $('#table-loader').hide();
        $('#table-loader').fadeIn();
        $('div.table-container').load('table', function () {
            $('#table-loader').hide();
            $('div.table-container').fadeIn();
        });


    }


    /*Eliminar*/
    $("#remove-form").submit(function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "cliente/" + $('#remove-id').attr("value"),
            dataType: 'json',
            data: $(this).serialize(),
            beforeSend: function () {
                $('#item-not-found').remove();
                $('#destroy-loading-bar').show();
            },
            success: function (data) {
                $success = data.responseJSON;
                $.notify({
                    // options
                    icon: 'fa fa-check',
                    title: '<strong>Success</strong>: <br>',
                    message: data['msg']
                }, {
                    // settings
                    type: "success",
                    allow_dismiss: true,
                    newest_on_top: true,
                    showProgressbar: false,
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 9999,
                    delay: 5000,
                    timer: 1000,
                    mouse_over: "pause",
                    animate: {
                        enter: 'animated fadeInDown',
                        exit: 'animated fadeOutUp'
                    }
                });
                $('#destroy-item').modal('toggle').click();
                recargar();
            },
            error: function (data) {
                $.notify({
                    // options
                    icon: 'fa fa-exclamation-triangle',
                    title: '<strong>Error</strong>: <br>',
                    message: 'An error occurred while getting data.'
                }, {
                    // settings
                    type: "danger",
                    allow_dismiss: true,
                    newest_on_top: true,
                    showProgressbar: false,
                    placement: {
                        from: "top",
                        align: "right"
                    },
                    offset: 20,
                    spacing: 10,
                    z_index: 9999,
                    delay: 5000,
                    timer: 1000,
                    mouse_over: "pause",
                    animate: {
                        enter: 'animated fadeInDown',
                        exit: 'animated fadeOutUp'
                    }
                });
                $('#remove-data').hide();
                (function () {
                    var notFound = $('<div class="modal-body fade-text" id="item-not-found"><h1 class="text-center danger">☠</h1><h2 class="text-center">Item not found</h2></div>');
                    notFound.insertAfter('#remove-data');
                })();
            }, complete() {
                $('#destroy-loading-bar').hide();
            }
        });
    });

});
