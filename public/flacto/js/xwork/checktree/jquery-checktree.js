/**
 * jQuery littleTree
 *
 * @version  0.1
 * @author   Mikahil Matyunin <free.all.bums@gmail.com>
 */

/**
 * <ul id="tree">
 *   <li><label><input type="checkbox" />Item1</label></li>
 *   <li>
 *     <label><input type="checkbox" />ItemWithSubitems</label>
 *     <ul>
 *       <li><label><input type="checkbox" />Subitem1</label></li>
 *     </ul>
 *   </li>
 * </ul>
 *
 * Usage:
 *
 * $('ul#tree').checktree();
 *
 */
(function($){
    $.fn.extend({

        checktree: function(){
            $(this)
                .addClass('checktree-root')
                .on('change', 'input[type="checkbox"]', function(e){
                    e.stopPropagation();
                    e.preventDefault();

                    checkParents($(this));
                    checkChildren($(this));
                })
            ;
            $(this).on('click', 'li input+span',function(e){
               var submenu = $(this).closest('li').find('>ul');
//               if(submenu.is('.chk-hide')){
//                   submenu.removeClass('chk-hide').addClass('chk-show');
//               } else {
//                   submenu.removeClass('chk-show').addClass('chk-hide');
//               }
               console.log($(submenu));
            });

            var checkParents = function (c)
            {
                if(c.is(':checked')){
                    c.parent().addClass('checked');
                } else {
                    c.parent().removeClass('checked');
                }
                
                var parentLi = c.parents('ul:eq(0)').parents('li:eq(0)');

                if (parentLi.length)
                {
                    var siblingsChecked = parseInt($('input[type="checkbox"]:checked', c.parents('ul:eq(0)')).length),
                        rootCheckbox = parentLi.find('input[type="checkbox"]:eq(0)')
                    ;

                    if (c.is(':checked')){
                        rootCheckbox.prop('checked', true);
                        rootCheckbox.parent().addClass('checked');
                    } else if (siblingsChecked === 0) {
                        rootCheckbox.prop('checked', false);
                        rootCheckbox.parent().removeClass('checked');
                    }

                    checkParents(rootCheckbox);
                }
            };

            var checkChildren = function (c)
            {
                var childLi = $('ul li input[type="checkbox"]', c.parents('li:eq(0)'));

                if (childLi.length){
                    if(c.is(':checked')){
                        childLi.prop('checked', true);
                        childLi.parent().addClass('checked');
                    } else {
                        childLi.prop('checked', false);
                        childLi.parent().removeClass('checked');
                    }
                }
            };
        }

    });
})(jQuery);
