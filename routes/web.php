<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|----------------------------------------------------------------------get_init----
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//init APP
Route::get('/', function () {
    return redirect('/admin/venta');
});
Route::fallback( function () {
    return redirect('/admin/venta');
} );
//Fin APP
//Módulo Venta
Route::prefix('admin')->group(function () {
    Route::resource('/venta', \App\Http\Controllers\Venta::class)->except(['destroy','store']);
    Route::get('/table_venta',  [\App\Http\Controllers\Venta::class, 'getTable']);
    Route::get('/venta/get',[\App\Http\Controllers\Venta::class, 'show']);
    Route::get('/edit/{id}',[\App\Http\Controllers\Venta::class, 'edit']);
    Route::post('/venta/detalle/add',[\App\Http\Controllers\Venta::class, 'createDetalle']);
    Route::post('/venta/getvalue',[\App\Http\Controllers\Venta::class, 'getValue']);
    Route::post('/venta/create',[\App\Http\Controllers\Venta::class, 'store']);
    Route::post('/venta/actualizar',[\App\Http\Controllers\Venta::class, 'actualizar']);
    Route::post('/venta/eliminar',[\App\Http\Controllers\Venta::class, 'eliminar']);
});




Route::get('create-pdf-boleta/{id}', [\App\Http\Controllers\PDFController::class, 'index']);
//Fin Módulo Venta



