<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVVentaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       DB::statement("CREATE VIEW v_venta AS select 
                        venta.id as id,
                        venta.fecha as fecha,
                        venta.iva as iva,
                        venta.descuento as descuento,
                        venta.total as total,
                        cliente.id as cliente_id,
                        cliente.estado as estado,
                        cliente.nombre as cliente
                        from venta
                        join cliente on cliente.id = venta.cliente_id");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::statement("DROP VIEW v_venta");
    }
}
