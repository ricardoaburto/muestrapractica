<?php

namespace App\Http\Controllers;

use App\Models\DetalleModel;
use App\Models\VentaModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;

class PDFController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $venta = DB::table('v_venta')->where(DB::raw('md5(id)'), $request->id)->first();
        $detalle = DB::table('detalle')
            ->join('producto', function ($join) {
                $join->on('producto.id', '=', 'detalle.producto_id');
            })->where('detalle.venta_id', '=', $venta->id)
            ->select( 'producto.nombre as producto','detalle.cantidad','detalle.subtotal as subtotal')->get();


        $data = [
            'date' => date('m/d/Y'),
            'cliente' => $venta->cliente,
            'fecha' => $venta->fecha,
            'subtotal' =>($venta->total - $venta->iva),
            'descuento' => $venta->descuento,
            'iva' => $venta->iva,
            'total' => $venta->total,
            'detalle' => $detalle->toArray() ,
        ];

        $pdf = PDF::loadView('boletaPDF', $data);
        $pdf->getDomPdf()->getOptions()->set('enable_php', true);
        return $pdf->download('boleta.pdf');
    }
}
