<?php

namespace App\Http\Controllers;

use App\ClienteModel;
use Illuminate\Http\Request;

class Cliente extends Controller
{
    public function index()
    {

        //$lists = ClienteModel::orderBy('id', 'desc')->get();

       // return view('admin/cliente.cliente')->withLists($lists);
       return view('admin/cliente.cliente', ['name' => 'James']);

    }

    /**
     * Display the specified resource.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function getTable()
    {
        $lists = ClienteModel::orderBy('id', 'desc')->where('activated', '=', 1)->get();
        $r = [];
        foreach ($lists as $key => $value) {
            $cliente = new \stdClass();
            $cliente->nombre = $value->nombre;
            $cliente->editar = '<button type="button" value="' . encrypt($value->id) . '" class="btn btn-primary btn-block btn-sm edit-btn" data-toggle="modal" data-target="#edit-item">Editar</button>';
            $cliente->eliminar = '<button type="button" value="' . encrypt($value->id) . '" class="btn btn-danger btn-block btn-sm remove-btn" data-toggle="modal" data-target="#destroy-item">Delete</button>';
            $r[] = $cliente;
        }
        $output["aaData"] = $r;
        return $output;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * Session::all()
     * Session::get()
     */
    public function store(Request $request)
    {
        $user = Cliente::create([
            'nombre' => $request->nombre,
        ]);

        $user->save();

        return response()->json([
            'status' => 'success',
            'msg' => 'Nuevo usuario ha sido creado'
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $list = ClienteModel::find($id);
        return response()->json([
            'status' => 'success',
            'idusuario' => $list->idusuario,
            'nombre' => 'required|max:255'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


        try {
            $idusuario = \Crypt::decrypt($id);
            $list = ClienteModel::find($idusuario);


            $array_tipo_movimiento = Usuario_tipo_moviModel::where('idusuario', '=', $idusuario)->where('activated', '=', 1)->get();
            $movimiento = $this->toArray($array_tipo_movimiento, 'idtipo_movimiento');

            $array_tipo_oficina = Usuario_oficinaModel::where('idusuario', '=', $idusuario)->where('activated', '=', 1)->get();
            $oficina = $this->toArray($array_tipo_oficina, 'idoficina');


            return response()->json([
                'status' => 'success',
                'idusuario' => encrypt($list->id),
                'nombre' => $list->nombre,
                'correspondencia' => !is_null($list->correspondencia) ? $list->correspondencia : 0,
                'idpunto_control' => $list->idpunto_control,
                'apellido' => $list->apellido,
                'tipo_movimiento' => $movimiento,
                'venta' => $oficina,
                'rut' => $list->rut,
                'idperfil' => $list->idperfil,
                'idpiso_oficina' => isset($list->idpiso_oficina) ? $list->idpiso_oficina : 0,
                'email' => $list->email,
                'remember_token' => $list->token,
                'data3' => $this->init_form2(),
                'select' => $this->select(),
                'selected' => $list->idperfil,
                'data4' => $this->init_form_movimiento()
            ]);


        } catch (Exception $e) {
            echo "<pre>";
            var_dump($e);
            die();
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {


            $array_piso = json_decode($request->piso);
            $array_movimiento = json_decode($request->movimiento);
            $variable_correspondencia = $request->correspondencia == 'on' ? 1 : 0;


            $faker = new Factory();
            $idusuario = Crypt::decrypt($id);
            $list = ClienteModel::find($idusuario);
            $list->nombre = $request->nombre;
            $list->apellido = $request->apellido;
            $list->rut = $request->rut;
            $list->email = $request->email;


            $list->idmodificador = Session::get('login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d');
            $list->idperfil = $request->idperfil;
            $list->correspondencia = $request->correspondencia == 'on' ? 1 : 0;
            if (!empty($request->pass)) {
                $list->password = Hash::make($request->pass);
            }
            $list->token = Hash::make($request->token);
            $list->updated_ip_address = $faker->create()->ipv4;
            $idretorno = $list->id;
            $list->save();


            $Usuario_tipo_moviModel = Usuario_tipo_moviModel::where('idusuario', '=', $idusuario)->get();

            foreach ($Usuario_tipo_moviModel as $kk => $vv) {
                $list1 = Usuario_tipo_moviModel::where('idusuario_tipo_movi', '=', $vv->idusuario_tipo_movi)->first();
                $list1->activated = 0;
                $list1->save();
            }


            $TaUsuarioOficinaPivot = TaUsuarioOficinaPivot::where('idusuario', '=', $idusuario)->get();

            foreach ($TaUsuarioOficinaPivot as $kkk => $vvv) {
                $list1 = TaUsuarioOficinaPivot::where('idusuario_oficina', '=', $vvv->idusuario_oficina)->first();
                $list1->activated = 0;
                $list1->save();
            }


            if (count($array_movimiento) > 0) {

                foreach ($array_movimiento as $key => $value) {


                    $use_movi = Usuario_tipo_moviModel::create(['idusuario' => $idretorno, 'idtipo_movimiento' => $value, 'activated' => 1, 'idmodificador' => Session::get('login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d')]);

                    $use_movi->save();
                }
            }


            if (count($array_piso) > 0) {

                foreach ($array_piso as $key => $value1) {

                    $use_movi = TaUsuarioOficinaPivot::create(['idusuario' => $idretorno, 'idoficina' => $value1, 'activated' => 1, 'idmodificador' => Session::get('login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d')]);

                    $use_movi->save();
                }
            }


            return response()->json([
                'status' => 'success',
                'msg' => 'El usuario ha sido actualizado'
            ]);

        } catch (Exception $e) {
            echo '<pre>';
            print_r($e);
            die();
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $faker = new Factory();
        $idusuario = Crypt::decrypt($id);
        $list = ClienteModel::find($idusuario);
        $list->deleted_ip_address = $faker->create()->ipv4;
        $list->ideliminador = Session::get('login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d');
        $list->activated = false;
        $list->save();

        return response()->json([
            'status' => 'success',
            'msg' => 'El usuario ha sido eliminado'
        ]);
    }


    public function toArray($array, $id)
    {

        $a = [];
        foreach ($array as $key => $value) {

            array_push($a, $value->$id);
        }

        return $a;
    }

    public function init_form()
    {


        $html = '';
        $html .= '<ul id="treeview">
                               <li> <i class="fa fa-plus"></i>
                                          <label>
                                             Edificios 
                                           </label>
                                                <ul>';

        $edificio = TaEdificio::where('idarray_edificio', '=', 1)->orderByRaw('idedificio ASC')->get();
        foreach ($edificio as $key1 => $value1) {
            $html .= ' <li> <i class="fa fa-plus"></i>
                              <label>
                                  ' . $value1->descripcion . '
                              </label>
                                   <ul>';

            $piso = TaPiso::where('idedificio', '=', $value1->idedificio)->orderByRaw('idpiso ASC')->get();
            foreach ($piso as $key2 => $value2) {
                $html .= ' <li> <i class="fa fa-plus"></i>
                                  <label>
                                    ' . $value2->descripcion . '
                                  </label>
                                   <ul>';
                $oficina = TaOficina::where('idpiso', '=', $value2->idpiso)->orderByRaw('idpiso ASC')->get();
                foreach ($oficina as $key3 => $value3) {
                    $html .= '<li>
                                        <label>
                                      <div class="checkbox checkbox-primary m-b-15">      <input   data-id="' . $value2->idpiso . '"  data-todo="' . $value2->idpiso . '" class="hummingbirdNoParent"  value="' . $value3->idoficina . '"  data-venta type="checkbox" /> 
                                       <label for="idoficina_' . $value2->idpiso . '">' . $value3->descripcion . '</label>
                                        </div></label>
                                    </li>';
                }

                $html .= '    </ul>
                                </li>';

            }


            $html .= '    </ul>
                        </li>';
        }
        $html .= '        </ul>
                    </li>                                 
                 </ul>';


        return response()->json([
            'status' => 'success',
            'data' => $html,
            'select' => $this->select(),
            'data2' => $this->init_form_movimiento()

        ]);

    }


    public function init_form2()
    {


        $html = '';
        $html .= '<ul id="treeview1">
                               <li> <i class="fa fa-plus"></i>
                                          <label>
                                             Edificios 
                                           </label>
                                                <ul>';

        $edificio = TaEdificio::where('idarray_edificio', '=', 1)->orderByRaw('idedificio ASC')->get();
        foreach ($edificio as $key1 => $value1) {
            $html .= ' <li> <i class="fa fa-plus"></i>
                              <label>
                                  ' . $value1->descripcion . '
                              </label>
                                   <ul>';

            $piso = TaPiso::where('idedificio', '=', $value1->idedificio)->orderByRaw('idpiso ASC')->get();
            foreach ($piso as $key2 => $value2) {
                $html .= ' <li> <i class="fa fa-plus"></i>
                                  <label>
                                    ' . $value2->descripcion . '
                                  </label>
                                   <ul>';
                $oficina = TaOficina::where('idpiso', '=', $value2->idpiso)->orderByRaw('idpiso ASC')->get();
                foreach ($oficina as $key3 => $value3) {
                    $html .= '<li>
                                        <label>
                                      <div class="checkbox checkbox-primary m-b-15">      <input data-id="' . $value3->idoficina . '" data-todo="' . $value2->idpiso . '" class="hummingbirdNoParent"  value="' . $value3->idoficina . '"  data-venta type="checkbox" /> 
                                       <label for="idoficina_' . $value2->idpiso . '">' . $value3->descripcion . '</label>
                                        </div></label>
                                    </li>';
                }

                $html .= '    </ul>
                                </li>';

            }


            $html .= '    </ul>
                        </li>';
        }
        $html .= '        </ul>
                    </li>                                 
                 </ul>';


        return $html;

    }

    public function select()
    {
        $perfil = TaPerfil::where('activated', '=', 1)->get();
        $r='';
        $r.= '<option value=""></option>';
        foreach ($perfil as $key =>$value ){
            $r.= '<option value="'.$value->idperfil.'">'.$value->nombre.'</option>';
        }

        return $r;


    }
}
