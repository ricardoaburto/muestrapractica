<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\VentaModel;
use \App\Models\ProductoModel;
use \App\Models\DetalleModel;
use App\Models\v_venta;
use App\Http\Controllers\Response;
use Mockery\Exception;

class Venta extends Controller
{
    public function index()
    {

        return view('admin/venta.venta', ['name' => 'James']);
    }

    public function getTable()
    {
        $aColumns = array('venta.id', 'nombre', 'fecha', 'total');
        $noShowNum = 1;
        $sIndexColumn = "id";
        $aWhere = [];
        $iTotal = DB::table('v_venta')
            ->select(DB::raw('count(id) as id'))
            ->where($aWhere)
            ->get();

        $sLimit = NULL;
        $sOrder = NULL;

        $m = DB::table('venta')
            ->join('cliente', function ($join) {
                $join->on('cliente.id', '=', 'venta.cliente_id');
            })->select('venta.id as id', 'cliente.nombre', 'venta.fecha', 'venta.total');

        if (isset($_GET['iSortCol_0'])) {
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i]) + $noShowNum] . " " . addslashes($_GET['sSortDir_' . $i]) . ", ";
                }
            }
            $sOrder = substr_replace($sOrder, "", -2);
            $m->orderByRaw($sOrder);
        }

        if ($_GET['sSearch'] != "") {
            for ($i = 0; $i < count($aColumns); $i++) {

                $m->orWhere($aColumns[$i], 'LIKE', '%' . addslashes($_GET['sSearch']) . '%');
            }
        }

        for ($i = 0; $i < count($aColumns); $i++) {
            if ($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
                $m->orWhere($aColumns[$i], 'LIKE', '%' . addslashes($_GET['sSearch_' . $i]) . '%');
            }
        }

        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            $data = $m->skip(addslashes($_GET['iDisplayStart']))->take(addslashes($_GET['iDisplayLength']))->get();
        } else {
            $data = $m->get();
        }
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => $iTotal[0]->id,
            "iTotalDisplayRecords" => $iTotal[0]->id,
            "aaData" => array()
        );

        for ($j = 0; $j < count($data); $j++) {
            $aRow = $data[$j];
            $row = array();
            for ($i = $noShowNum; $i < (count($aColumns)); $i++) {
                $row[] = $aRow->{$aColumns[$i]};
            }

            $r = "";
            $r .= '<div class="btn-group" data-dataid="' . md5($aRow->{$sIndexColumn}) . '">
                        <button class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-gear"></i>  <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu fixed-dropdown-menu1 pull-right">
                            <li>
                                <a class="txt-color-green" href="#" onclick="App.editar(\'' . md5($aRow->{$sIndexColumn}) . '\');return false;"><i class="fa fa-edit"></i> Editar</a>
                            </li>';
            $r .= ' <li>
                                <a class="txt-color-red" href="#" onclick="App.pdf(\'' . md5($aRow->{$sIndexColumn}) . '\');return false;"><i class="fa fa-trash-o"></i> Detalle PDF</a>
                            </li>';
            $r .= ' <li>
                                <a class="txt-color-red" href="#" onclick="App.eliminar(\'' . md5($aRow->{$sIndexColumn}) . '\');return false;"><i class="fa fa-trash-o"></i> Eliminar</a>
                            </li>';

            $r .= ' </ul>
                      </div>';
            $row[] = $r;

            $output['aaData'][] = $row;
        }
        return json_encode($output);
    }

    public function show()
    {
        $cliente = DB::table('cliente')->where('estado', '=', 1)->get();
        $producto = DB::table('producto')->where('cantidad', '>', 0)->get();
        return Response::JSONDATA(['cliente' => $this->getSelectOptions($cliente, 'id', 'nombre', false, 0), 'producto' => $this->getSelectOptions($producto, 'id', 'nombre', false, 0)]);
    }

    public function createDetalle()
    {
        $m = DB::table('detalle');
        $m->join('producto', 'producto.id', '=', 'detalle.producto_id');
        $m->select('SUM(detalle.cantidad)');
        $m->where('detalle.producto_id', $_POST['idproducto']);

        $stockTablaDetalle = DetalleModel::select( DB::raw('SUM(detalle.cantidad) As sumcantidad'))
            ->leftJoin('producto', 'producto.id', '=', 'detalle.producto_id')
            ->where('detalle.producto_id', $_POST['idproducto'])
            ->first();

        $productoTotal = DB::table('producto')->where('id', $_POST['idproducto'])->first();

        $html = "";
        $stock = 0;
        $stockDis = 0;

        if ((($stockTablaDetalle->sumcantidad + $_POST['total_cantidad']) <= $productoTotal->cantidad )) {
            $producto = DB::table('producto')->where('id', $_POST['idproducto'])->where('cantidad', '>=', $_POST['total_cantidad'])->first();
            $productoDisponible = DB::table('producto')->where('id', $_POST['idproducto'])->first();

            $con = ($_POST['contador'] + 1);
            if ($producto) {
                $stock = 1;
                $html = '<tr data-id="' . $con . '"  data-precio="' . $producto->precio . '"  data-idproducto="' . $producto->id . '"  data-cantidad="' . $_POST['cantidad'] . '">
                    <td>' . $con . '</td>
                    <td>' . $producto->nombre . '</td>
                    <td>' . $_POST['cantidad'] . '</td>
                    <td>$' . number_format(($_POST['cantidad'] * $producto->precio), 0, ',', '.') . '</td>
                    <td>
                        <button class="btn btn-icon waves-effect waves-light btn-danger btn-xs m-b-2" type="button" onclick="App.eliminarDetalle(' . $con . ');">eliminar</button>
                    </td>
                 </tr>';
            }


            if ($productoDisponible) {
                if ($productoDisponible->cantidad > $_POST['total_cantidad']) {
                    $stockDis = ($productoDisponible->cantidad - $_POST['total_cantidad']);
                } elseif ($productoDisponible->cantidad < $_POST['total_cantidad']) {
                    $dif = $productoDisponible->cantidad - ($_POST['total_cantidad'] - $_POST['cantidad']);
                    $stockDis = $dif;
                }
            }
        }
        return Response::JSONDATA(['data' => $html, 'stock' => $stock, 'stockDisponible' => $stockDis]);
    }

    public function eliminar()
    {
        try {
            //eliminal no posee activo
            DB::table('detalle')->where(DB::raw('md5(venta_id)'), $_POST['id'])->delete();
            DB::table('venta')->where(DB::raw('md5(id)'), $_POST['id'])->delete();
            return Response::JSONTRUE;
        } catch (Exception $e) {
            return Response::JSONFALSE;
        }

    }

    public function getValue()
    {
        try {
            $producto = ProductoModel::where('id', $_POST['id'])->first();

            return Response::JSONDATA(['data' => $producto->precio]);
        } catch (Exception $e) {
            return Response::JSONFALSE;
        }
    }

    public function store()
    {
        try {
            $this->guardar($_POST);
            return Response::JSONTRUE;
        } catch (Exception $e) {
            return Response::JSONFALSE;
        }


    }

    public function guardar($p)
    {
        $data = json_decode($p['arrayVenta']);
        $venta = new VentaModel();
        $venta->fecha = $this->now();
        $venta->iva = str_replace(',', '.', str_replace('.', '', $p['iva']));
        $venta->descuento = $p['descuento'];
        $venta->total = str_replace(',', '.', str_replace('.', '', $p['total']));
        $venta->cliente_id = $p['idcliente'];
        $venta->save();
        $venta_id = $venta->id;
        foreach ($data as $item) {
            $producto = DB::table('producto')->where(DB::raw('id'), $item->producto_id)->first();
            $detalle = new DetalleModel();
            $detalle->venta_id = $venta_id;
            $detalle->cantidad = $item->cantidad;
            $detalle->producto_id = $item->producto_id;
            $detalle->subtotal = $producto->precio * $item->cantidad;
            $detalle->save();
        }
    }


    public function edit($id)
    {
        $venta = DB::table('venta')->where(DB::raw('md5(id)'), $id)->first();

        $detalle = DB::table('detalle')
            ->join('producto', function ($join) {
                $join->on('producto.id', '=', 'detalle.producto_id');
            })->where('detalle.venta_id', '=', $venta->id)
            ->select('detalle.*', 'producto.nombre as producto', 'producto.id as idproducto', 'producto.precio as precio')->get();

        $html = "";
        $subTotal = 0;
        $total = $venta->total;
        $iva = $venta->iva;
        $descuento = $venta->descuento;
        if (count($detalle) > 0) {
            $con = 1;
            foreach ($detalle as $item) {
                $subTotal += $item->subtotal;
                $html .= '<tr data-id="' . $con . '"  data-precio="' . $item->precio . '"  data-idproducto="' . $item->idproducto . '"   data-cantidad="' . $item->cantidad . '">
                    <td>' . $con . '</td>
                    <td>' . $item->producto . '</td>
                    <td>' . $item->cantidad . '</td>
                    <td>$ ' . number_format(($item->cantidad * $item->precio), 0, ',', '.') . '</td>
                  
                    <td>
                        <button class="btn btn-icon waves-effect waves-light btn-danger btn-xs m-b-2" type="button" onclick="App.eliminarDetalle(' . $con . ');">eliminar</button>
                    </td>
                 </tr>';
                $con++;
            }
        } else {
            $html .= '<tr data-id="0" data-precio="0" data-cantidad="0"  data-idproducto="0">
            <td></td>
            <td></td>
            <td>Sin Productos</td>
            <td></td>
            <td></td>
            </tr>
            ';
        }
        return Response::JSONDATA(['cliente' => $venta->cliente_id, 'tabla' => $html, 'subtotal' => $subTotal, 'total' => $total, 'iva' => $iva, 'descuento' => $descuento]);
    }


    public function actualizar()
    {
        try {

            //eliminal no posee activo
            DB::table('detalle')->where(DB::raw('md5(venta_id)'), $_POST['idventa'])->delete();
            DB::table('venta')->where(DB::raw('md5(id)'), $_POST['idventa'])->delete();
            $this->guardar($_POST);
            return Response::JSONTRUE;
        } catch (Exception $e) {
            return Response::JSONFALSE;
        }

    }
}
