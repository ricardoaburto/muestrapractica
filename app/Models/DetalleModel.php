<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetalleModel extends Model
{
    use HasFactory;
    protected $table = 'detalle';
    protected $primaryKey = 'id';
    protected $visible = ['venta_id','venta_id','cantidad','producto_id','producto_id','subtotal'];
    public $timestamps = false;
}
